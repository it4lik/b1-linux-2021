# Mémo réseau Rocky

Vous trouverez ici quelques mini-procédures pour réaliser certaines opérations récurrentes. Ce sera évidemment principalement utilisé pour notre cours de réseau, mais peut-être serez-vous amenés à le réutiliser plus tard.  

**Ces mini-procédures sont écrites pour un système Rocky Linux**. Elles ne sont pas forcément applicables à d'autres distributions. C'est toujours le même concept, peu importe l'OS, mais parfois des façons différentes de faire.

La plupart des éléments sont directement transposables à d'autres OS de la famille RedHat (CentOS, Fedora, entre autres).

# Sommaire

<!-- vim-markdown-toc GitLab -->

- [Mémo réseau Rocky](#mémo-réseau-rocky)
- [Sommaire](#sommaire)
- [Définir une IP statique](#définir-une-ip-statique)
- [Définir une IP dynamique (DHCP)](#définir-une-ip-dynamique-dhcp)
- [Changer son nom de domaine](#changer-son-nom-de-domaine)
- [Editer le fichier hosts](#editer-le-fichier-hosts)
- [Interagir avec le firewall](#interagir-avec-le-firewall)
- [Configurer l'utilisation d'un serveur DNS](#configurer-lutilisation-dun-serveur-dns)

<!-- vim-markdown-toc -->

---

# Définir une IP statique

➜ **1. Repérer le nom de l'interface dont on veut changer l'IP**

```
ip a
```

➜ **2. Modifier le fichier correspondant à l'interface**

- il se trouve dans `/etc/sysconfig/network-scripts`
- il porte le nom `ifcfg-<NOM_DE_L'INTERFACE>`
- on peut le créer s'il n'existe pas
- exemple de fichier minimaliste qui assigne `192.168.1.19/24` à l'interface `enp0s8`
  - c'est donc le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8`

```
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=192.168.1.19
NETMASK=255.255.255.0

# La suite est optionnelle
GATEWAY=192.168.1.254
DNS1=1.1.1.1
```

➜ **3. Redémarrer l'interface**

```
sudo nmcli con reload
sudo nmcli con up enp0s8
```

# Définir une IP dynamique (DHCP)

➜ **1. Repérer le nom de l'interface dont on veut changer l'IP**

```
ip a
```

➜ **2. Modifier le fichier correspondant à l'interface**

- il se trouve dans `/etc/sysconfig/network-scripts`
- il porte le nom `ifcfg-<NOM_DE_L'INTERFACE>`
- on peut le créer s'il n'existe pas
- exemple de fichier minimaliste qui assigne `192.168.1.19/24` à l'interface `enp0s8`
  - c'est donc le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8`

```bash
NAME=enp0s8
DEVICE=enp0s8

BOOTPROTO=dhcp
ONBOOT=yes
```

➜ **3. Redémarrer l'interface**

```bash
sudo nmcli con reload
sudo nmcli con up <INTERFACE_NAME>
```

# Changer son nom de domaine

➜ **1. Changer le nom d'hôte immédiatement** (temporaire)

```bash
# commande hostname
sudo hostname <NEW_HOSTNAME>
# par exemple
sudo hostname vm1.tp3.b1
```

➜ **2. Définir un nom d'hôte quand la machine s'allume** (permanent)

- écriture du nom d'hôte dans le fichier (avec `nano`) : `sudo nano /etc/hostname`
- **OU** en une seule commande `echo 'vm1.tp1.b3' | sudo tee /etc/hostname`

➜ **3. Pour consulter votre nom d'hôte actuel**

```bash
hostname
```

# Editer le fichier hosts

Le fichier `hosts` se trouve au chemin `/etc/hosts`. Sa structure est la suivante :

- une seule IP par ligne
- une ligne est une correspondance entre une IP et un (ou plusieurs) noms (nom d'hôte)
- on peut définir des commentaires avec `#`  

Par exemple, pour faire correspondre l'IP `192.168.1.19` aux noms `monpc` et `monpc.chezmoi` :

```bash
192.168.1.19  monpc monpc.chezmoi
```

- on peut tester le fonctionnement avec un `ping`

```bash
ping monpc.chezmoi
```

# Interagir avec le firewall

Rocky Linux est aussi équipé d'un pare-feu. Par défaut, il bloque tout, à part quelques services comme `ssh`. Le firewall de Rocky Linux s'appelle `firewalld`.

> `firewalld` peut autoriser/bloquer des ports ou des "services". Les "services" sont juste des alias pour des ports. Par exemple le "service" SSH c'est le port 22/tcp. 

Pour manipuler le firewall de Rocky Linux, on utilise la commande `firewall-cmd` :

- `sudo firewall-cmd --list-all` pour lister toutes les règles actives actuellement
- `sudo firewall-cmd --add-port=80/tcp --permanent` pour autoriser les connexions sur le port TCP 80 
- `sudo firewall-cmd --remove-port=80/tcp --permanent` pour supprimer une règle qui autorisait les connexions sur le port TCP 80 
- `sudo firewall-cmd --reload` permet aux modifications effectuées de prendre effet

# Configurer l'utilisation d'un serveur DNS

Un serveur DNS est un serveur capable de traduire des noms de domaines en adresses IP.  

Souvent, toutes les machines d'un parc connaissent un serveur DNS à qui poser leurs questions.  

Sous GNU/Linux c'est le fichier `/etc/resolv.conf` qui est utilisé pour renseigner l'adresse du serveur DNS à interroger si besoin. On peut le modifier à volonté. 

Exemple d'utilisation de `1.1.1.1` (serveur DNS public de CloudFlare) comme serveur DNS :

```bash
# Contenu du fichier
$ sudo cat /etc/resolv.conf
nameserver 1.1.1.1

# Tester le bon fonctionnement
## En effectuant une requête HTTP vers un nom de domaine avec curl
$ curl gitlab.com

## En effectuant directement une requête DNS avec dig
$ dig gitlab.com
```

Il est aussi possible d'ajouter `DNS1=1.1.1.1` dans le fichier de configuration d'interface `/etc/sysconfig/network-scripts/ifcfg-<INTERFACE_NAME>`. [Voir la section dédiée](#définir-une-ip-statique).
