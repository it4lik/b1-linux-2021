# SSH

![I will SSH you](./pics/i_will_find_you.jpg)

# Sommaire

- [SSH](#ssh)
- [Sommaire](#sommaire)
- [I. Intro](#i-intro)
- [II. La connexion SSH](#ii-la-connexion-ssh)
  - [1. Commande ssh](#1-commande-ssh)
  - [2. Commande scp](#2-commande-scp)
- [III. Les fichiers SSH](#iii-les-fichiers-ssh)
  - [1. Côté serveur](#1-côté-serveur)
  - [2. Côté client](#2-côté-client)
  - [3. Known Hosts et empreinte](#3-known-hosts-et-empreinte)
    - [A. De quoi on parle ?](#a-de-quoi-on-parle-)
    - [B. Empreinte ?](#b-empreinte-)
    - [C. L'utilité dans le cadre de SSH](#c-lutilité-dans-le-cadre-de-ssh)
- [IV. Echange de clés](#iv-echange-de-clés)
  - [1. Intro](#1-intro)
  - [2. Le principe](#2-le-principe)
  - [3. Dans les faits](#3-dans-les-faits)
    - [A. Génération de clé par le client](#a-génération-de-clé-par-le-client)
    - [B. Le client dépose sa clé sur le serveur](#b-le-client-dépose-sa-clé-sur-le-serveur)
    - [C. Le client se connecte](#c-le-client-se-connecte)
- [V. Résumé quick](#v-résumé-quick)

# I. Intro

> En prérequis de ce cours sur SSH, il est **nécessaire** d'avoir assimilé [**les termes de *client* et de *serveur*** définis dans ce cours-notion](../../notions/serveur/README.md).

***SSH* (*Secure SHell*) est un protocole permettant de prendre le contrôle d'une machine à distance.** Plus précisément, on va prendre le contrôle sur un terminal d'une machine distante.

**IL FAUT COMPRENDRE** et démystifier ça de suite. SSH faut pas en avoir peur, vous devez vous en servir naturellement.  
**SSH c'est juste un truc qui permet de se co à une machine à distance, sans avoir besoin d'être physiquement devant.**

> Comme TeamViewer pour ceux qui connaissent, sauf qu'on récup qu'un terminal, et pas toute la partie graphique.

Une fois connecté au terminal d'une machine, on a accès à tout : **on contrôle complètement la machine**.

# II. La connexion SSH

**La connexion SSH est une relation client/serveur.**

C'est à dire que l'**on a d'un côté le serveur**. Le serveur lance un service SSH, qui écoute derrière un port.  
Par convention les serveurs SSH écoutent derrière le port 22 en TCP.

> Sur GNU/Linux, le programme qui est lancé est le binaire `sshd`. Le paquet qui le contient est souvent `openssh-server`.

**De l'autre côté, il y a le client**. Le client utilise un programme dédié, un *client SSH*, pour se connecter au serveur.  
Comme tout client sur le réseau, il doit connaître l'adresse IP et le port auxquels il souhaite se connecter.

La commande `ssh`, désormais disponible sous tous les OS, est un *client SSH*.

> Le port c'est souvent 22 comme mentionné au dessus, par convention. Le client connaît donc souvent déjà le port auquel il faut se connecter. Ne lui reste plus qu'à connaître l'adresse.

**La connexion SSH permet d'ouvrir une nouvelle session utilisateur** sur la machine qui porte le serveur lorsqu'un client se connecte.  
**Ainsi, le client doit fournir le nom de l'utilisateur** sur lequel il souhaite se connecter, ainsi que le mot de passe dudit utilisateur.

> Là normalement je ne vous apprends rien : quand on ouvre une session sur un PC, on doit saisir le nom d'un utilisateur qui existe sur le PC, et le mot de passe associé. Vous le faites tout le temps quand vous allumez les vôtres de PC, non ?

## 1. Commande ssh

En supposant que :

- on connaît un serveur dont l'IP est 192.168.1.1
- on sait qu'il y a un service SSH qui tourne dessus, derrière le port 22
- on sait qu'il y un utilisateur `toto` sur la machine
- on connaît le mot de passe de cet utilisateur

ALORS, on peut se connecter en SSH en tapant la commande suivante :

```bash
$ ssh toto@192.168.1.1 -p 22
# Le port 22 est implicite, car c'est la convention
# On peut donc simplement utiliser :
$ ssh toto@192.168.1.1
```

## 2. Commande scp

`scp` c'est pour Ssh CoPy. Une copie de fichiers à travers SSH, dingue non ?

Cela permet de transférer de façon simple, rapide et sécurisé des fichiers entre deux machines. Plus précisément, on peut :

- envoyer un fichier local sur une machine à distance
- récupérer un fichier d'une machine distante en local
- récupérer un fichier d'une machine distante et l'envoyer sur une autre machine distance

BREF c'est un simple copier-coller. Mais il passe par SSH, car SSH ça permet de le faire à travers le réseau de façon sécurisée.

On suppose que :

- vous avez une connexion SSH fonctionnelle vers un serveur distant
  - avec une commande `ssh user@IP` typiquement
  - on supposera `toto` pour l'utilisateur et 192.168.1.1 pour l'IP dans l'exemple
- vous voulez récupérer un fichier `/srv/toto.mp3` sur le serveur, et le mettre dans `C:/Users/plip/music/toto.mp3`
- vous voulez envoyer votre fichier `C:/Users/plip/documents/plap.pdf` dans le dossier `/home/toto/documents/`

On pourra alors exécuter les deux commandes suivantes :

```bash
$ scp toto@192.168.1.1:/srv/toto/mp3 C:/Users/plip/music/toto.mp3
$ scp C:/Users/plip/documents/plap.pdf toto@192.168.1.1:/home/toto/documents/

# De façon générale la syntaxe est la suivante
# Comme d'habitude, ce qui est entre crochet, c'est optionnel
# Attention à l'utilisation des : entre une chaîne SSH et un chemin
# Attention aux espaces : 'scp source destination' (que deux espaces)
$ scp [user@IP:]/chemin/vers/source [user@IP:]/chemin/vers/destination
```

# III. Les fichiers SSH

Il existe plein de fichiers, sur le serveur comme sur le client, qui peuvent influer sur le comportement des connexions SSH.

## 1. Côté serveur

Côté serveur, les fichiers de configuration sont dans `/etc/ssh/`. Le fichier de conf principal est `/etc/ssh/sshd_config`.

On va ici pouvoir changer le comportement ddu serveur SSH (le port sur lequel il écoute par exemple).

## 2. Côté client

Deux dossiers pour le client.

➜ **D'abord `/etc/ssh/ssh_config`** qui contient les informations globales aux connexions SSH initiées par la machine cliente, peu importe l'utilisateur qui effectue la connexion.

On s'est pas trop attardés là-dessus en cours, allez vous-mêmes étancher votre soif de curiosité.

➜ **On va ensuite trouver un dossier `.ssh/` pour chaque utilisateur de la machine.** Dossier qui se trouvera dans le répertoire personnel de chaque utilisateur. Ce dossier s'appelle `.ssh/` et il est présent sous tous les OS.

> Ainsi, si votre utilisateur, sous GNU/Linux, s'appelle `toto`, le dossier `.ssh/` de `toto` sera dans `/home/toto/.ssh/`.

Ce dossier `.ssh/` contient de façon récurrente (rien d'obligatoire) 3 fichiers :

- clé privée (la clé), souvent `id_rsa`
- clé publique (le cadenas), souvent `id_rsa.pub`
- un fichier `known-hosts`

Nous allons tâcher de comprendre ces fichiers dans le reste du document.

## 3. Known Hosts et empreinte

### A. De quoi on parle ?

La toute toute touuuuute première fois qu'on se co en SSH à une machine, une phrase de ce genre apparaît, côté client :

```
$ ssh toto@192.168.1.1
The authenticity of host '192.168.1.1' can't be established.
RSA key fingerprint is c1:91:5e:42:21:3c:74:65:bc6:12:32:7e:af:6d:80:3e.
Are you sure you want to continue connecting (yes/no)?
```

### B. Empreinte ?

En traduisant, on comprend que l'on nous demande si l'on souhaite accepter l'empreinte `c1:91:5e:42:21:3c:74:65:bc6:12:32:7e:af:6d:80:3e` du serveur.

L'empreinte du serveur SSH est une chaîne de caractère qui identifie le serveur de façon unique. L'empreinte est strictement différente pour tous les serveurs SSH du monde.

Il n'est pas possible normalement, grâce à des procédés cryptographiques, d'usurper l'empreinte d'un serveur SSH.

### C. L'utilité dans le cadre de SSH

![You shall not paaaaaaaaass](./pics/you_shall_not_pass.jpg)

➜ **Ainsi, à la première connexion, on nous demande d'accepter l'empreinte du serveur.**  

**L'idée c'est que le serveur essaie de nous prouver que c'est bien lui**, et pas un potentiel hacker sur le réseau, qui se fait passer pour notre serveur SSH.

> Comme dit et répété en cours, le hacking, c'est pas que dans les séries Netflix.

En tant qu'administrateurs éclairés, nous sommes en mesure d'aller (physiquement) sur la machine qui héberge le serveur SSH, et **consulter localement l'empreinte du serveur SSH**.

Dans un deuxième temps, on effectue une connexion SSH vers ce serveur, il nous fournit son empreinte, et nous sommes en mesure de **comparer avec celle que l'on avait récolté localment sur le serveur**.

Si l'empreinte que nous propose le serveur SSH est bien celle qu'on avait récupérée grâce à une connexion physique, alors, nous discutons avec le bon serveur : **le serveur nous a prouvé son identité.**

Simple nan ? : D

➜ **Une fois l'empreinte acceptée par l'humain (en saisissant `yes` puis `Entrée`), elle est enregistrée par la machine cliente.**

En effet, cela sera utilse si l'on essaie de se reconnecter une nouvelle fois à ce serveur SSH :

- lors d'une nouvelle connexion au serveur SSH, celui-ci nous donnera à nouveau son empreinte pour qu'on vérifie son identité.  
- sauf que notre machine a enregistré cette info à la première connexion.  
- donc le programme client comparera automatiquement pour nous l'empreinte stockée la première fois, avec la nouvelle qui est fournie par le serveur lors de l'établissement de cette nouvelle connexion

Si :

- je suis connecté à une machine "Machine A"
- je suis connecté sur "Machine A" avec l'utilisateur `toto`
- je me connecte en SSH, depuis "Machine A", vers "Machine B" qui porte l'ip `192.168.1.1`
- alors l'empreinte du serveur SSH de "Machine B" sera stockée dans le fichier `/home/toto/.ssh/known_hosts` de "Machine A"

➜ **Si l'empreinte fournie est différente de celle stockée à la première connexion**

Alors il y a un message qui fait peur qui s'affiche :

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that the RSA host key has just been changed.
The fingerprint for the RSA key sent by the remote host is
c1:91:5e:42:21:3c:74:65:bc6:12:32:7e:af:6d:80:3f.
Please contact your system administrator.
Add correct host key in /home/toto/.ssh/known_hosts to get rid of this message.
Offending key in /home/toto/.ssh/known_hosts:6
RSA host key for 192.168.1.1 has changed and you have requested strict checking.
Host key verification failed. 
```

Cela signifie que le serveur auquel on s'adresse n'est plus le même que lors de la première connexion. Plusieurs raisons possibles :

- le serveur a subi une grosse mise à jour
- le serveur a carrément été réinstallé
- vous parlez à un hacker

Si l'on a connaissance d'une raison légitime pour laquelle l'empreinte du serveur a changé, il est parfaitement possible de supprimer l'ancienne empreinte (récupérée à la première connexion) afin de lever le *Warning* et établir la connexion.

> Pour supprimer l'empreinte, ça se passe dans le fichier `known_hosts`. Le fichier contient une empreinte par ligne.  
C'est écrit dans le message de *Warning* à quelle ligne du fichier se trouve l'empreinte concernée. Dans l'exemple ci-dessus : `Offending key in /home/toto/.ssh/known_hosts:6` (c'est donc à la ligne 6).

# IV. Echange de clés

Bon je le glisse ici parce qu'on va s'en servir TOUT LE TEMPS pour SSH, mais c'est un procédé utilisé dans + de contextes que ça en réalité.

## 1. Intro

L'échange de clés désigne un procédé qui permet à un utilisateur de prouver son identité.

La façon traditionnelle IRL pour prouver son identité, c'est la signature.  
Je pense qu'on peut tous convenir qu'une signature c'est naze, puisque vous avez déjà imité celles de vos parents quad vous aviez 12 ans.

Y'a une autre méthode très connue, et très utilisée en info, c'est l'utilisation d'un mot de passe. C'est à dire une string que vous seul connaissez.

Bon on a pas appris grand chose là encore. è_é

**MAIS, en info, on a une troisième façon de gérer le problème : *la signature numérique*, qui est possible grâce à un *échange de clés*.**

## 2. Le principe

Si quelqu'un veut prouver son identité à des gens, à des serveurs hein essentiellement, il va générer une paire de clé.

> Sur une machine GNU/Linux, ça se fait par exemple avec la commande `ssh-keygen`, on verra ça un peu plus bas.

Une des deux clés agit vraiment comme une clé, c'est la clé privée.  
Pour la simplicité de l'explication, l'autre clé, la clé publique, nous considérerons que c'est un cadenas.

La clé et le cadenas générés par le client sont uniques. Ainsi, seule sa clé ouvre son cadenas.

Son cadenas, le client le donne à tout le monde. Le cadenas (la clé publique) n'est pas secret. Ainsi, tout le monde possède le cadenas (même de potentiels hackers, balec, ça compromet pas la sécurité du procédé).  
Dans notre cas, le serveur SSH auquel on se connectera plus tard a donc en sa possession notre cadenas, car on lui donne.

Si un jouuuuuuuuuuuuuuuuuur on essaie de se connecter à ce serveur SSH à distance, alors, plutôt que d'utiliser un mot de passe, on pourra fournir notre clé, et ça suffira à prouver notre identité. And boom.

PORKOA ME DIREZ-VOUS ? Simple :

- j'essaie de me connecter à un serveur sur lequel j'ai déposé un cadenas auparavant
- le serveur capte qu'on essaie de se connecter sur tel utilisateur
- il voit qu'il y a un cadenas qui a été déposé sur cet utilisateur
- il va créer un message, un "challenge", qui sera fermé (il sera "chiffré" en réalité) avec MON cadenas
- il m'envoie ce message chiffré avec MON cadenas
- je reçois le message chiffré avec MON cadenas
- JE SUIS LA SEULE PERSONNE A POUVOIR OUVRIR LE MESSAGE car je suis le seul qui possède la clé associée au cadenas

Ainsi, tout hacker sur le réseau ne pourra pas déchiffrer ce message, et donc ne pourra pas se faire passer pour moi car il n'a pas ma clé.

Boom. Plus besoin de mot de passe, que des échanges de clé partout. Ca permet d'éviter les erreurs humaines liées aux mot de passe + reposer sur des procédés cryptographiques robustes et forts.

## 3. Dans les faits

Ca se fait en trois étapes, simple as fuck :

- le client génère uen clé et un cadenas
  - c'est comme s'il créait un nouveau mot de passe quoi
- le client pose son cadenas aux endroits où il veut se co plus tard
  - sur tous les serveurs où il va se co quoi
  - c'est comme si vous créiez un compte sur tous ces serveurs, avec un mot de passe
  - sauf que les mots de passe, c'est de la merde, donc go échange de clés
- le client se connecte
  - en réussissant à déchiffrer le message chiffré par le serveur avec le cadenas du client
  - sans fournir de mot passe

### A. Génération de clé par le client

La commande pour générée des clés est `ssh-keygen`. Pour générer des clés considérées comme robustes à la date de rédaction de ce document, on utilise les options suivantes :

```
$ ssh-keygen -t rsa -b 4096
```

> Lorsqu'on tape cette commande, on nous demande un chemin où stocker la clé. Il est bon de laisser le chemin par défaut, pour que nos programmes trouvent facilement notre clé.

La clé s'enregistre par défaut dans `/home/<USER>/.ssh/id_rsa` et le cadenas (la clé publique) `/home/<USER>/.ssh/id_rsa.pub`.

On peut utiliser un même couple de clé + cadenas pour plusieurs serveurs (ce serait comme définir le même mot de passe).

### B. Le client dépose sa clé sur le serveur

Avec SSH, ça se fait avec la commande `ssh-copy-id`. En supposant que je veux me connecter sur la machine `192.168.1.1` avec l'utilisateur `john`, je peux saisir :

```bash
$ ssh-copy-id john@192.168.1.1
```
Cela aura pour effet de :

- me demander le mot de passe de `john`
  - bah oui, on utilise encore le mot de passe, puisqu'on a pas fini de config l'échange de clés !
- stocker mon cadenas dans `/home/john/.ssh/authorized_keys` sur la machine `192.168.1.1`
- m'afficher un message comem quoi je peux désormais me connecter sans mot de passe

> Notez que l'on peut faire cette opération manuellement, en allant sur la machine `192.168.1.1` et en créant le fichier `/home/john/.ssh/authorized_keys` à la main, en y ajoutant notre cadenas sur une nouvelle ligne. Les permissions de ce fichier sont rédhibitoires pour que SSH accepte de les utiliser.

### C. Le client se connecte

Bah écoutez : `ssh john@192.168.1.1`. Baboom.

![Use SSH keys](./pics/use_ssh_keys.jpg)

# V. Résumé quick

➜ **[Un serveur SSH](#ii-la-connexion-ssh) c'est un programme qui permet aux clients qui se connectent de contrôler le serveur à l'aide d'un terminal.**  
SSH est donc un protocole idéal pour l'administration de machines GNU/Linux.   
Un serveur SSH écoute derrière le port 22 par convention.

➜ **[Un client](#ii-la-connexion-ssh) se connecte en utilisant un programme appelé client SSH**, comme la commande `ssh`.  
Il doit pour cela connaître l'adresse IP et le port auquel il souhaite se connecter.  
Il doit aussi connaître le nom d'utilisateur et le mot de passe associé d'un compte sur la machine distante, sur le serveur.

➜ **Il existe un système d'[empreintes](#3-known-hosts-et-empreinte), pour que le serveur puisse prouver son identité aux clients.**

➜ **Il est possible de mettre en place [un échange de clé](#iv-echange-de-clés), pour que le client prouve son identité au serveur**, de façon plus sûre et robuste qu'avec un simple mot de passe.

➜ Commandes à taper pour l'achange de clés :

```bash
# Sur le client : On suppose que la commande suivante fonctionne
# mais elle nous demande le mot de passe de l'utilisateur :
$ ssh john@192.168.1.1

# Sur le client : Génération de clé
$ ssh-keygen -t rsa -b 4096

# Sur le client : On dépose la clé sur le serveur
$ ssh-copy-id john@192.168.1.1

# Sur le client : Connexiooooon sans password
$ ssh john@192.168.1.1
```

➜ Fichiers liés à SSH sur le serveur :

- `/etc/ssh/sshd_config` : fichier de conf principal
- `/home/USER/.ssh/authorized_keys` : la liste des cadenas autorisé à se connecter sur ce USER

➜ Fichiers liés à SSH sur le client :

- `/home/USER/.ssh/known_hosts` : la liste des serveurs auxquels on s'est déjà co, avec l'empreinte associée
- `/home/USER/.ssh/id_rsa` : notre clé
- `/home/USER/.ssh/id_rsa.pub` : notre cadenas

![SSH is not bad](./pics/not_bad.jpg)
