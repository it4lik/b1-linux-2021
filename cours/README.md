# Cours

## ➜ Cours

- [Introduction à Linux](./cours/intro/README.md)
  - Relation entre le matériel, le noyau, l'OS, le shell et l'utilisateur
  - Linux
  - GNU
  - Libre et Open-Source
- [Les fonctions de l'OS](./cours/os/README.md)
- [Le FHS](./cours/FHS/README.md)
- [SSH](./cours/SSH/README.md)
- [Principe du moindre privilège](./cours/least_privilege_principle/README.md)

## ➜ [Notions](./notions/README.md)

Mini-cours sur des notions précises.

- [Permissions POSIX (`rwx`)](./notions/permissions/README.md)
- [Les termes "Serveur", "Client" et "Service"](./notions/serveur/README.md)
- [La notion de "port" en réseau](./notions/port/README.md)
- [L'encodage](./notions/encodage/README.md)
- [Les *filesystems*](./notions/filesystem/README.md)
- [Les flux dans le terminal, et le redirections de flux](./notions/flux/README.md)
- [Git](./notions/git/README.md)

## ➜ Memos

- [Mémo commandes GNU/Linux](./memos/commandes.md)
- [Mémo réseau Rocky](./memos/rocky_network.md)
- [Mémo LVM](./memos/lvm.md)