# Flux et redirection de flux

- [Flux et redirection de flux](#flux-et-redirection-de-flux)
  - [Intro](#intro)
  - [Flux de sortie](#flux-de-sortie)
  - [Flux d'entrée](#flux-dentrée)
  - [Redirection de flux](#redirection-de-flux)
    - [Caractère `>`](#caractère-)
    - [Caractère `|`](#caractère--1)

## Intro

On parle ici des flux liés à l'exécution de commande dans un terminal `bash`.

En effet, lorsque l'on exécute une commande dans un shell `bash`, cette commande peut écrire dans le terminal du texte en sortie : ce sont les flux de sortie.

La commande peut aussi prendre des données en arguments, qui seront envoyées dans son flux d'entrée.

Par exemple, la commande `ls`, qui sert à liste les fichiers d'un dossier, écrit en sortie dans le terminal la liste des fichiers.

![Flux](./pics/flux.png)

## Flux de sortie

Une commande possède deux sorties où elle peut écrire :

- **`STDOUT` **ou **Sortie Standard**
  - si la commande se déroule correctement, sans erreurs, elle écrira dans la sortie standard
- **`STDERR`** ou **Sortie d'Erreur**
  - si la commande ne se déroule pas correctement, les messages d'erreurs seront écrit dans las sortie d'erreur

Par exemple :

```bash
# la commande va bien se passer, le message sera écrit dans la sortie standard
$ ls 
Documents   Musique   Téléchargements

# la commande va retourner une erreur, le message sera écrit dans la sortie d'erreur
$ ls /dossier/qui/nexiste/pas
ls: cannot access '/dossier/qui/nexiste/pas': No such file or directory
```

## Flux d'entrée

Certaines commandes peuvent prendre en entrée des données.

Une des façons d'envoyer des données à une commande, si elle le supporte, c'est d'utiliser **l'entrée standard de la commande** ou **`STDIN`**.

Pour envoyer des données dans l'entrée d'une commande on utilise souvent la syntaxe `command1 | command2`.

Ainsi, le texte qui sort en sortie standard `STDOUT` de la commande `command1` sera envoyé en entrée standard `STDIN` de la `command2`.

On utilise par exemple très souvent la commande `grep` de cette façon. La commande `grep` permet de filtrer du texte. Elle est donc idéale pour isoler juste certaines lignes de sortie losque l'on exécute une commande avant.

Exemple :

```bash
# Liste tous les ports en écoute sur la machine
$ sudo ss -alntp

# Isole la ou les lignes qui contiennent la string "22"
# Pour isoler la ligne du serveur SSH typiquement :)
$ sudo ss -alntp | grep "22"
```

## Redirection de flux

Plusieurs caractères peuvent être utilisés dans le terminal afin de rediriger les flux de sortie d'une commande ou d'envoyer des données en entrée d'une autre commande : ce sont les caractères `>`, `<` et `|`.

### Caractère `>`

Le caractère `>` permet de rediriger les flux de sortie, `STDOUT` comme `STDERR` :

- `>` seul permet de rediriger `STDOUT` vers un fichier, en écrasant son contenu
- `>>` permet de rediriger `STDOUT` vers un fichier, en ajoutant à la fin du fichier (plutôt que d'écraser)
- `2>` permet de rediriger `STDERR` vers un fichier, en écrasant son contenu
- je vous laisse deviner ce que fait `2>>`
- on peut utiliser `&>` pour rediriger `STDOUT` et `STDERR` en même temps

Exemples :

```bash
$ ls 
Documents   Musique   Téléchargements

$ ls > coucou

# Rien ne s'affiche dans le terminal, la sortie de la commande ls a été redirigée
# Un nouveau fichier coucou a été créé, et contient la sortie de la commande ls

$ ls
Documents   Musique   Téléchargements   coucou

$ cat coucou
Documents   Musique   Téléchargements
```

### Caractère `|`

Le caractère `|` est utlisé pour passer la sortie standard d'une commande (`STDOUT`) dans l'entrée standard (`STDIN`) d'une autre.

Exemple, bah le même exemple qu'au dessus :

```bash
# Liste tous les ports en écoute sur la machine
$ sudo ss -alntp

# Isole la ou les lignes qui contiennent la string "22"
# Pour isoler le serveur SSH typiquement :)
$ sudo ss -alntp | grep "22"
```

Autre exemple, pour la route :

```bash
# Liste des processus
$ ps -ef

# Trouver le processus qui s'appelle firefox
$ ps -ef | grep firefox
```
