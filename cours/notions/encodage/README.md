# Encodage

- [Encodage](#encodage)
- [I. Intro](#i-intro)
- [II. Les bases en mathématiques](#ii-les-bases-en-mathématiques)
- [III. Encodages très utilisés](#iii-encodages-très-utilisés)
  - [**Le binaire**](#le-binaire)
  - [**Le décimal**](#le-décimal)
  - [**L'hexadecimal**](#lhexadecimal)
  - [**La base 64**](#la-base-64)
  - [**L'ASCII**](#lascii)
  - [**Y en a plein d'autres**](#y-en-a-plein-dautres)

![You decoded it wrooong](./pics/decoded_it_wrong.jpg)

# I. Intro

**L'encodage est une technique permettant de représenter une même information de différentes façons.**

L'encodage n'amène aucune notion de *secret*. L'encodage n'est pas de la cryptographie.

> Pour ce qui est des termes : il est possible de coder (*encode* en anglais) ou de décoder (*decode* en anglais) une information.

Les procédés basiques d'encodage reposent sur le concept de ***bases*** en mathématiques.  

# II. Les bases en mathématiques

**Une *base* en mathématiques, c'est le nombre de caractère que l'on s'autorise à utiliser dans notre système numéraire.**

**Dans la vie de tous les jours, on compte en *base 10***. C'est à dire que pour compter, on a 10 caractères différents : de 0 à 9.

> Les historiens et sociologues estiment qu'on compte en *base 10* tous les jours car on est dotés de 10 doigts, et que c'est avec les doigts et de petites quantité qu'est né le fait de dénombrer les choses. Quel beau primate.

**Si on change de *base*, on ne change aucune règle arithmétique, on change juste la façon de représenter les nombres.**

> On précise la base en petit par rapport au nombre, en bas à droite de celui-ci (y'a des exemples plus bas). On met parfoit des parenthèses autour.

Par exemple pour l'addition, les règles sont simples (ouais j'vais rien vous apprendre là, mais c'est pour expliquer le machin).  
Si on décrit le comportement de l'addition en *base 10 :*

```math
0_{10} + 1_{10} = 1_{10}
```

```math
1_{10} + 1_{10} = 2_{10}
```

```math
...
```

```math
8_{10} + 1_{10} = 9_{10}
```

```math
9_{10} + 1_{10} = 10_{10}
```

**Une fois arrivé au bout de nos caractères disponibles, on effectue une retenue. DINGUE NON ?!**

---

Mais il est possible d'imaginer ça avec n'importe quelle autre *base*.

Prenons un exemple avec la *base 6*, où l'on a droit à 6 caractères seulement, de 0 à 5 par exemple :

```math
0_6 + 1_6 = 1_6
```

```math
1_6 + 1_6 = 2_6
```

```math
2_6 + 1_6 = 3_6
```

```math
3_6 + 1_6 = 4_6
```

```math
4_6 + 1_6 = 5_6
```

```math
5_6 + 1_6 = 10_6
```

```math
10_6 + 1_6 = 11_6
```

```math
...
```

```math
14_6 + 1_6 = 15_6
```

```math
15_6 + 1_6 = 20_6
```

```math
20_6 + 1_6 = 21_6
```

**On change pas les règles : une fois arrivé au bout de nos caractères disponibles, on effectue une retenue. DINGUE NON ?!**

Pour préciser la base, on utilise des parenthèses

---

On peut ainsi dire, par exemple :

```math
1_6 = 1_{10}
```

```math
2_6 = 2_{10}
```

```math
5_6 = 5_{10}
```

```math
10_6 = 6_{10}
```

```math
12_6 = 8_{10}
```

```math
15_6 = 11_{10}
```
```math
20_6 = 12_{10}
```

# III. Encodages très utilisés

## **Le binaire**

C'est la *base 2* : caractères de `0` à `1`.

Elle est particulièrement utile pour stocker ou faire transiter des informations.

> Ceci est dû à la façon dont on stocke et fait transiter les informations numériques.

## **Le décimal**

C'est la *base 10* : caractères de `0` à `9`.

Très utilisée par les humains.

## **L'hexadecimal**

C'est la *base 16* : caractères de `0` à `9` puis de `a` à `f`.

Ainsi :

```math
0_{16} + 1_{16} = 1_{16}
```

```math
1_{16} + 1_{16} = 2_{16}
```

```math
2_{16} + 1_{16} = 3_{16}
```

```math
...
```

```math
9_{16} + 1_{16} = A_{16}
```

```math
A_{16} + 1_{16} = B_{16}
```

```math
...
```

```math
E_{16} + 1_{16} = F_{16}
```

```math
F_{16} + 1_{16} = 10_{16}
```

Très utilisée car 16 est une puissance de 2. Et que le binaire est très utilisé.

> C'est simplement pratique pour un ordinateur de stocker du binaire, et la façon la plus "humaine" de le représenter, ce sera de l'hexadécimal.

## **La base 64**

C'est *hum*, la *base 64* è_é.

Là encore, c'est une puissance de 2. La base64 est donc utile pour représenter des données binaires de façon plus humaine.

Caractères :

- `0` à `9`
- `a` à `z`
- `A` à `Z`
- `+`, `=`, `/`

> avec le caractère `=` ça fait 65 en fait OMG. Ouais on se sert de ce caractère pour terminer les chaînes de caractères en *base64*. Genéralement, une string cheloue, composées des caractères cités au dessus, et qui se termine par `=` ou `==`, c'est de la *base64*.

![Is this base64 ?](./pics/is_this_base64.jpg)

## **L'ASCII**

La table ASCII de base, c'est la *base 128*.

![Table ASCII](./pics/ascii_table.png)

La table ASCII étendue, c'est la *base 256*.

![Table ASCII étendue](./pics/ascii_extended_table.png)

## **Y en a plein d'autres**

Comme l'UTF-8, qui repose sur un principe un peu plus malin qu'une simple base, ou encore d'autres.

L'encodage est un sujet à part entière car il s'agit de pouvoir stocker, faire transiter et représenter tous les caractères que le monde connaît.  
Notre alphabet bien sûr mais aussi des caractères cyrilliques, des idéogrammes chinois, l'alphabet arabe, оυ ԁ'аυτгеѕ τгυϲѕ ϲһеⅼоυѕ. Hihi.