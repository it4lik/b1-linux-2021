# B1 Linux 2021

Ici seront déposés tous les supports de cours liés au cours Linux 1ère année 2021.

### ➜ [**Cours**](./cours/README.md)

- [Introduction à Linux](./cours/cours/intro/README.md)
- [Les fonctions de l'OS](./cours/cours/os/README.md)
- [Le FHS](./cours/cours/FHS/README.md)
- [SSH](./cours/cours/SSH/README.md)
- [Principe du moindre privilège](./cours/cours/least_privilege_principle/README.md)

#### ➜ [**Notions**](./cours/notions/README.md)

- [Permissions POSIX](./cours/notions/permissions/README.md)
- [Les termes "Serveur", "Client" et "Service"](./cours/notions/serveur/README.md)
- [La notion de "port" en réseau](./cours/notions/port/README.md)
- [L'encodage](./cours/notions/encodage/README.md)
- [Les *filesystems*](./cours/notions/filesystem/README.md)
- [Les flux dans le terminal, et le redirections de flux](./cours/notions/flux/README.md)
- [Git](./cours/notions/git/README.md)

#### ➜ [**Memos**](./cours/memos/)

- [Mémo commandes Linux](./cours/memos/commandes.md)
- [Mémo réseau Rocky](./cours/memos/rocky_network.md)
- [Mémo LVM](./cours/memos/lvm.md)

### ➜ [**TP**](./tp/README.md)

- [TP1 : Are you dead yet ?](./tp/1/README.md)
- [TP2 : Manipulation de services](./tp/2/README.md)
- [TP3 : Scripting](./tp/3/README.md)
- [TP4 : Une distribution orientée serveur](./tp/4/README.md)
- [TP5 : P'tit cloud perso](./tp/5/README.md)
- [TP6 : Stockage et sauvegarde](./tp/6/README.md)
