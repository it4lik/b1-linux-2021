# TP4 : Une distribution orientée serveur

Maow dans ce TP on va setup quelque chose qui ressemble un peu plus à un serveur de la vraie vie.

Un des principaux trucs qu'on va jeter c'est l'interface graphique : à quoi ça sert de cliquer dans 40 menus sur des machins quand on peut faire la même chose en une seule ligne de commande puissante ?

Qui dit serveur dit servir, on mettra en place un serveur Web dans la deuxième partie du TP.

# Sommaire

- [TP4 : Une distribution orientée serveur](#tp4--une-distribution-orientée-serveur)
- [Sommaire](#sommaire)
- [I. Install de Rocky Linux](#i-install-de-rocky-linux)
- [II. Checklist](#ii-checklist)
- [III. Mettre en place un service](#iii-mettre-en-place-un-service)
  - [1. Intro NGINX](#1-intro-nginx)
  - [2. Install](#2-install)
  - [3. Analyse](#3-analyse)
  - [4. Visite du service web](#4-visite-du-service-web)
  - [5. Modif de la conf du serveur web](#5-modif-de-la-conf-du-serveur-web)

# I. Install de Rocky Linux

![Install Linux](./pics/install_linux.jpg)

Rocky Linux est une distribution orientée robustesse, sécurité et stabilité. Elle est idéale pour des serveurs.  
C'est une distribution de la famille RedHat.

> xubuntu ou juste Ubuntu, c'est  de la famille Debian.

Vous allez réaliser une install de RockyLinux. Les paramètres à changer lors de l'install :

- langue : anglais
- clavier : azerty (enfin, en accord avec votre clavier quoi)
- allumer la carte NAT
- créer un utilisateur administrateur et lui donner un mot de passe
- donner un mot de passer à root

Maintenant, petit détails de config : allumez la VM, loggez vous puis exécutez :

```bash
# On désactive un truc qui s'appelle SELinux sur lequel je vais pas m'étendre
$ sudo sed -i 's/enforcing/permissive/g' /etc/selinux/config

# Profitez en aussi pour mettre la machine à jour.
# Dans les systèmes RedHat, le gestionnaire de paquets c'est dnf, pas apt
$ sudo dnf update -y
```

Eteginez maintenant la VM, elle est prête à être clonée à chaque fois qu'on a besoin d'une machine Rocky Linux.

# II. Checklist

![Checklist is here !](./pics/checklist_is_here.jpg)

A chaque fois que vous créerez une machine Rocky Linux, vous vérifierez les éléments de la checklist qui suit.

Pour cette première fois, vous me rendrez dans le compte-rendu les étapes que vous avez réalisé pour mener ça à bien. Dans les futurs TPs, la checklist devra simplement être réalisée à chaque fois.

**Créez donc une nouvelle machine** (clonez celle qu'on vient d'install). On l'appellera `node1.tp4.linux`.

---

➜ **Configuration IP statique**

Jusqu'à maintenant on laissait notre machine récupérer une IP automatiquement. Pour des clients, c'est ce qui se passe dans la vie réelle (genre ton smartphone connecté à ta box, il récup une IP automatiquement).

**Pour un serveur, c'est différent : on veut que son IP soit prévisible, donc fixe. On la définit donc à la main.**

Dans Rockly Linux, on définit la configuration des cartes réseau dans le dossier `/etc/sysconfig/network-scripts/`. Il existe là-bas (on peut le créer s'il n'existe pas), un fichier par interface réseau.

Par exemple, pour l'interface `enp0s8`, le fichier de configuration se nommera `ifcfg-enp0s8`, au chemin`/etc/sysconfig/network-scripts/ifcfg-enp0s8`.

Le contenu que vous devez utiliser :

```
NAME=enp0s8          # nom de l'interface
DEVICE=enp0s8        # nom de l'interface
BOOTPROTO=static     # définition statique de l'IP (par opposition à DHCP)
ONBOOT=yes           # la carte s'allumera automatiquement au boot de la machine
IPADDR=<IP_CHOISIE>  # adresse IP choisie
NETMASK=<MASK>       # masque choisi
```

A chaque fois que vous modifiez un fichier de configuration d'interface, il faudra exécuter :

```bash
# on indique au système que les fichiers ont été modifiés
$ sudo nmcli con reload

# on allume/reload l'interface avec la nouvelle conf
$ sudo nmcli con up <INTERFACE>
# par exemple
$ sudo nmcli con up enp0s8
```

> Ceci est aussi mentionné dans [le mémo réseau Rocky Linux](../../cours/memos/rocky_network.md)

🌞 **Choisissez et définissez une IP à la VM**

- vous allez devoir configurer l'interface host-only
- ce sera nécessaire pour pouvoir SSH dans la VM et donc écrire le compte-rendu
- je veux, dans le compte-rendu, le contenu de votre fichier de conf, et le résultat d'un `ip a` pour me prouver que les changements on pris effet

---

➜ **Connexion SSH fonctionnelle**

Vous pouvez vous connecter en SSH à la VM. Cela implique :

- la VM a une carte réseau avec une IP locale
- votre PC peut joindre cette IP
- la VM a un serveur SSH qui est accessible derrière l'un des ports réseau

🌞 **Vous me prouverez que :**

- le service ssh est actif sur la VM
  - avec une commande `systemctl status ...`
- vous pouvez vous connecter à la VM, grâce à un échange de clés
  - référez-vous [au cours sur SSH pour + de détails sur l'échange de clés](../../cours/cours/SSH/README.md)

> Pour me prouver que vous pouvez vous connecter avec un échange de clé il faut me montrer, dans l'idéal : la clé publique (le cadenas) sur votre PC (un `cat` du fichier), un `cat` du fichier `authorized_keys` concerné sur la VM, et une connexion sans aucun mot de passe demandé.

---

➜ **Accès internet**

Par l'expression commune et barbare "avoir un accès internet" on entend deux choses généralement :

- *ahem* avoir un accès internet
  - c'est à dire être en mesure de ping des IP publiques
- avoir de la résolution de noms
  - la machine doit pouvoir traduire un nom comme `google.com` vers l'IP associée

🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`

> On utilise souvent un `ping` vers une adresse IP publique connue pour tester l'accès internet.  
Vous verrez souvent `8.8.8.8` c'est l'adresse d'un serveur Google. On part du principe que Google sera bien le dernier à quitter Internet, donc on teste l'accès internet en le pingant.  
Si vous n'aimez pas Google comme moi, vous pouvez `ping 1.1.1.1`, c'est les serveurs de CloudFlare.

🌞 **Prouvez que vous avez de la résolution de nom**

Un petit `ping` vers un nom de domaine, celui que vous voulez :)

---

➜ **Nommage de la machine**

Votre chambre vous la rangez na ? Au moins de temps en temps ? Là c'est pareil : on range les machines. Et ça commence par leur donner un nom.

Pour donner un nom à une machine il faut exécuter deux actions :

- changer son nom immédiatement, mais le changement sera perdu au reboot
- écrire son nom dans un fichier, pour que le fichier soit lu au boot et que le changement persiste

En commande ça donne :

```bash
# Pour la session
$ sudo hostname <NOM>

# Persistant après les reboots
$ sudo nano /etc/hostname # remplacer le contenu du fichier par le NOM
```

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

- montrez moi le contenu du fichier `/etc/hostname`
- tapez la commande `hostname` (sans argument ni option) pour afficher votre hostname actuel

> Vous verrez aussi votre hostname dans le prompt du terminal, en plus de l'utilisateur avec lequel vous êtes co : `it4@node1:~$`.  
C'est de l'anglais en fait hein, le "@" se dit "at". Donc en l'occurence "it4 at node1".

# III. Mettre en place un service

On va installer un service de test pour voir la procédure récurrente d'installation et configuration d'un nouveau service.  
C'est un peu la routine de l'administrateur système. My job heh.

Toujours les mêmes opérations :

- **install du service**
  - souvent via des paquets
- puis **conf du service**
  - dans des fichiers texte de configuration
- puis si c'est un service qui utilise le réseau, il faudra **ouvrir** un port dans le pare-feu
  - hé ui, fini les distribs comme xubuntu où c'est open-bar
  - Rocky Linux bloque la plupart du trafic qui essaie d'entrer sur la machine
- puis **lancement du service**
  - ptite commande `systemctl start` généralement
- puis **enjoy**
  - consommation du service par des clients

On va setup un serveur web. Cas d'école un peu, on va s'en servir pour approfondir encore notre maîtrise des services.

## 1. Intro NGINX

![gnignigggnnninx ?](./pics/ngnggngngggninx.jpg)

NGINX (prononcé "engine-X") est un serveur web. C'est un outil de référence aujourd'hui, il est réputé pour ses performances et sa robustesse.

Ici on va pas DU TOUT s'attarder sur la partie dév web étou, une simple page HTML fera l'affaire.

Une fois le serveur web installé, on récupère :

- **un service**
  - un service c'est un processus
  - il y a donc un binaire, une application qui fait serveur web
  - qui dit processus, dit que quelqu'un, un utilisateur lance ce processus
  - c'est l'utilisateur qu'on voit lister dans la sortie de `ps -ef`
- **des fichiers de conf**
  - comme d'hab c'est dans `/etc/` la conf
  - comme d'hab c'est bien rangé, donc la conf de NGINX c'est dans `/etc/nginx/`
  - question de simplicité en terme de nommage, le fichier de conf principal c'est `/etc/nginx/nginx.conf`
  - la conf, ça appartient à l'utilisateur `root`
- **une racine web**
  - c'est un dossier dans lequel le site est stocké
  - c'est à dire là où se trouvent tous les fichiers PHP, HTML, CSS, JS, etc du site
  - ce dossier et tout son contenu doivent appartenir à l'utilisateur qui lance le service
- **des logs**
  - tant que le service a pas trop tourné c'est empty
  - comme d'hab c'est `/var/log/`
  - comme d'hab c'est bien rangé donc c'est dans `/var/log/nginx/`
  - comme d'hab on peut aussi consulter certains logs avec `sudo journalctl -xe -u nginx`

## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**

Vous devez comprendre toutes les commandes que vous tapez. En deux trois commandes c'est plié l'install.

## 3. Analyse

Avant de config étou, on va lancer à l'aveugle et inspecter ce qu'il se passe.

Commencez donc par démarrer le service NGINX :

```bash
$ sudo systemctl start nginx
$ sudo systemctl status nginx
```

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX
- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web
- en regardant la conf, déterminer dans quel dossier se trouve la racine web
- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus

## 4. Visite du service web

Et ça serait bien d'accéder au service non ? Bon je vous laisse pas dans le mur : spoiler alert, le service est actif, mais le firewall de Rocky bloque l'accès au service, on va donc devoir le configurer.

Il existe [un mémo dédié au réseau au réseau sous Rocky](../../cours/memos/rocky_network.md), vous trouverez le nécessaire là-bas pour le firewall.

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** (c'est du TCP ;) )

🌞 **Tester le bon fonctionnement du service**

- avec votre navigateur sur VOTRE PC
  - ouvrez le navigateur vers l'URL : `http://<IP_VM>:<PORT>`
- vous pouvez aussi effectuer des requêtes HTTP depuis le terminal, plutôt qu'avec un navigateur
  - ça se fait avec la commande `curl`
  - et c'est ça que je veux dans le compte-rendu, pas de screen du navigateur :)

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- une simple ligne à modifier, vous me la montrerez dans le compte rendu
  - faites écouter NGINX sur le port 8080
- redémarrer le service pour que le changement prenne effet
  - `sudo systemctl restart nginx`
  - vérifiez qu'il tourne toujours avec un ptit `systemctl status nginx`
- prouvez-moi que le changement a pris effet avec une commande `ss`
- n'oubliez pas de fermer l'ancien port dans le firewall, et d'ouvrir le nouveau
- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080

---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`
- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur

---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
  - avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
  - le dossier et tout son contenu doivent appartenir à `web`
- configurez NGINX pour qu'il utilise cette nouvelle racine web
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

![Uuuuunpossible](./pics/nginx_unpossible.jpg)
