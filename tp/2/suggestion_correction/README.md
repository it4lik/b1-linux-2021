# TP2 : Explorer et manipuler le système

# Prérequis

## 1. Une machine xubuntu fonctionnelle

## 2. Nommer la machine

🌞 **Changer le nom de la machine**

```bash
it4@it4-vm:~$ sudo hostname node1.tp2.linux
it4@it4-vm:~$
it4@it4-vm:~$ sudo vim /etc/hostname
it4@it4-vm:~$ cat /etc/hostname
node1.tp2.linux
```

## 3. Config réseau

🌞 **Config réseau fonctionnelle**

```bash
it4@node1:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0d:c3:cc brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86197sec preferred_lft 86197sec
    inet6 fe80::9d9b:c3cc:a1a6:b6f5/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0b:2a:8f brd ff:ff:ff:ff:ff:ff
    inet 192.168.57.8/24 brd 192.168.57.255 scope global dynamic noprefixroute enp0s9
       valid_lft 397sec preferred_lft 397sec
    inet6 fe80::b359:fe55:c539:cf60/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

# L'IP de la VM est 192.168.57.8

# Ping 1.1.1.1 depuis la VM
it4@node1:~$ ping 1.1.1.1 -c 2
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 octets de 1.1.1.1 : icmp_seq=1 ttl=63 temps=27.2 ms
64 octets de 1.1.1.1 : icmp_seq=2 ttl=63 temps=29.4 ms

--- statistiques ping 1.1.1.1 ---
2 paquets transmis, 2 reçus, 0 % paquets perdus, temps 1001 ms
rtt min/avg/max/mdev = 27.173/28.264/29.356/1.091 ms

# Ping ynov.com depuis la VM
it4@node1:~$ ping ynov.com -c 2
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 octets de xvm-16-143.dc0.ghst.net (92.243.16.143) : icmp_seq=1 ttl=63 temps=22.8 ms
64 octets de xvm-16-143.dc0.ghst.net (92.243.16.143) : icmp_seq=2 ttl=63 temps=26.2 ms

--- statistiques ping ynov.com ---
2 paquets transmis, 2 reçus, 0 % paquets perdus, temps 1002 ms
rtt min/avg/max/mdev = 22.824/24.527/26.231/1.703 ms

# Ping de la VM depuis l'hôte
it4@node1:~$ exit
Connection to 192.168.57.8 closed.
[it4@nowhere 2]$ ping -c 2  192.168.57.8
PING 192.168.57.8 (192.168.57.8) 56(84) bytes of data.
64 bytes from 192.168.57.8: icmp_seq=1 ttl=64 time=0.369 ms
64 bytes from 192.168.57.8: icmp_seq=2 ttl=64 time=0.510 ms

--- 192.168.57.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1026ms
rtt min/avg/max/mdev = 0.369/0.439/0.510/0.070 ms
```

# Partie 1 : SSH

# II. Setup du serveur SSH

## 1. Installation du serveur

🌞 **Installer le paquet `openssh-server`**

```bash
it4@node1:~$ sudo apt install openssh-server -y
Lecture des listes de paquets... Fait
Construction de l'arbre des dépendances
Lecture des informations d'état... Fait
openssh-server est déjà la version la plus récente (1:8.2p1-4ubuntu0.3).
0 mis à jour, 0 nouvellement installés, 0 à enlever et 107 non mis à jour.
```

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

```bash
# Démarrage du service ssh
it4@node1:~$ sudo systemctl start ssh

# Affichage de l'état du service ssh
it4@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-16 10:57:42 CET; 11min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 578 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 587 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 4.0M
     CGroup: /system.slice/ssh.service
             └─587 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

nov. 16 10:57:41 it4-vm sshd[587]: Server listening on :: port 22.
nov. 16 10:57:42 it4-vm systemd[1]: Started OpenBSD Secure Shell server.
nov. 16 10:58:01 it4-vm sshd[1277]: Accepted password for it4 from 192.168.57.1 port 43538 ssh2
nov. 16 10:58:01 it4-vm sshd[1277]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 16 11:02:56 node1.tp2.linux sshd[1385]: Accepted password for it4 from 192.168.57.1 port 43540 ssh2
nov. 16 11:02:56 node1.tp2.linux sshd[1385]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 16 11:05:02 node1.tp2.linux sshd[1477]: Accepted password for it4 from 192.168.57.1 port 43542 ssh2
nov. 16 11:05:02 node1.tp2.linux sshd[1477]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 16 11:07:50 node1.tp2.linux sshd[1594]: Accepted password for it4 from 192.168.57.1 port 43544 ssh2
nov. 16 11:07:50 node1.tp2.linux sshd[1594]: pam_unix(sshd:session): session opened for user it4 by (uid=0)

# On demande au système de lancer le service ssh lorsque la machine démarre
it4@node1:~$ sudo systemctl enable ssh
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
```

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*

```bash
it4@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-16 10:57:42 CET; 24min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 587 (sshd)
      Tasks: 1 (limit: 2312)
     Memory: 4.0M
     CGroup: /system.slice/ssh.service
             └─587 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

nov. 16 10:57:41 it4-vm sshd[587]: Server listening on :: port 22.
nov. 16 10:57:42 it4-vm systemd[1]: Started OpenBSD Secure Shell server.
nov. 16 10:58:01 it4-vm sshd[1277]: Accepted password for it4 from 192.168.57.1 port 43538 ssh2
nov. 16 10:58:01 it4-vm sshd[1277]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
```

- afficher le/les processus liés au *service* `ssh`

```bash
it4@node1:~$ ps -ef | grep ssh
root         587       1  0 11:01 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
it4          945     862  0 11:01 ?        00:00:00 /usr/bin/ssh-agent /usr/bin/im-launch startxfce4
root        1594     587  0 11:07 ?        00:00:00 sshd: it4 [priv]
it4         1646    1594  0 11:07 ?        00:00:00 sshd: it4@pts/1
it4         1795    1647  0 11:22 pts/1    00:00:00 grep --color=auto ssh
```

- afficher le port utilisé par le *service* `ssh`

```bash
it4@node1:~$ sudo ss -alnpt | grep sshd
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*        users:(("sshd",pid=587,fd=3))
LISTEN    0         128                   [::]:22                  [::]:*        users:(("sshd",pid=587,fd=4))
# On voit que le service ssh écoute derrière le port 22
```

- afficher les logs du *service* `ssh`

```bash
it4@node1:~$ journalctl -xe -u ssh
nov. 09 16:14:50 it4-vm sshd[1275]: Connection closed by authenticating user it4 192.168.57.1 port 42736 [preauth]
nov. 09 16:14:59 it4-vm sshd[1277]: Accepted password for it4 from 192.168.57.1 port 42738 ssh2
nov. 09 16:14:59 it4-vm sshd[1277]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 09 16:15:16 it4-vm sshd[1365]: Connection closed by authenticating user it4 192.168.57.1 port 42740 [preauth]
nov. 09 16:15:34 it4-vm sshd[1369]: Accepted password for it4 from 192.168.57.1 port 42742 ssh2
nov. 09 16:15:34 it4-vm sshd[1369]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 09 16:15:43 it4-vm sshd[1431]: Accepted password for it4 from 192.168.57.1 port 42744 ssh2
nov. 09 16:15:43 it4-vm sshd[1431]: pam_unix(sshd:session): session opened for user it4 by (uid=0)
nov. 09 16:21:11 it4-vm sshd[1520]: Failed password for it4 from 192.168.57.1 port 42746 ssh2
nov. 09 16:40:57 it4-vm sshd[1630]: Accepted password for it4 from 192.168.57.1 port 42748 ssh2
[...]

# La commande tail -n 20 permet de regarder les 20 dernières lignes d'un fichier
it4@node1:/var/log$ tail -n 20 /var/log/auth.log
Nov 16 11:08:23 it4-vm sudo: pam_unix(sudo:session): session closed for user root
Nov 16 11:09:00 it4-vm sudo:      it4 : TTY=pts/1 ; PWD=/home/it4 ; USER=root ; COMMAND=/usr/bin/systemctl start ssh
Nov 16 11:09:00 it4-vm sudo: pam_unix(sudo:session): session opened for user root by it4(uid=0)
Nov 16 11:09:00 it4-vm sudo: pam_unix(sudo:session): session closed for user root
Nov 16 11:09:20 it4-vm sudo:      it4 : TTY=pts/1 ; PWD=/home/it4 ; USER=root ; COMMAND=/usr/bin/systemctl enable ssh
Nov 16 11:09:20 it4-vm sudo: pam_unix(sudo:session): session opened for user root by it4(uid=0)
Nov 16 11:09:21 it4-vm sudo: pam_unix(sudo:session): session closed for user root
Nov 16 11:17:01 it4-vm CRON[1782]: pam_unix(cron:session): session opened for user root by (uid=0)
Nov 16 11:17:01 it4-vm CRON[1782]: pam_unix(cron:session): session closed for user root
Nov 16 11:22:53 it4-vm pkexec: pam_unix(polkit-1:session): session opened for user root by (uid=1000)
Nov 16 11:22:53 it4-vm pkexec[1862]: it4: Executing command [USER=root] [TTY=unknown] [CWD=/home/it4] [COMMAND=/usr/lib/update-notifier/package-system-locked]
Nov 16 11:23:50 it4-vm sudo:      it4 : TTY=pts/1 ; PWD=/home/it4 ; USER=root ; COMMAND=/usr/bin/ss -alnpt
Nov 16 11:23:50 it4-vm sudo: pam_unix(sudo:session): session opened for user root by it4(uid=0)
Nov 16 11:23:50 it4-vm sudo: pam_unix(sudo:session): session closed for user root
Nov 16 11:23:54 it4-vm sudo:      it4 : TTY=pts/1 ; PWD=/home/it4 ; USER=root ; COMMAND=/usr/bin/ss -alnpt
Nov 16 11:23:55 it4-vm sudo: pam_unix(sudo:session): session opened for user root by it4(uid=0)
Nov 16 11:23:55 it4-vm sudo: pam_unix(sudo:session): session closed for user root
Nov 16 11:26:00 it4-vm sudo:      it4 : TTY=pts/1 ; PWD=/var/log ; USER=root ; COMMAND=/usr/bin/grep -nri sshd
Nov 16 11:26:00 it4-vm sudo: pam_unix(sudo:session): session opened for user root by it4(uid=0)
Nov 16 11:26:01 it4-vm sudo: pam_unix(sudo:session): session closed for user root
```

🌞 **Connectez vous au serveur**

```bash
[it4@nowhere 2]$ ssh 192.168.57.8
it4@192.168.57.8's password: 
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

112 mises à jour peuvent être appliquées immédiatement.
51 de ces mises à jour sont des mises à jour de sécurité.
Pour afficher ces mises à jour supplémentaires, exécuter : apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Tue Nov 16 11:07:51 2021 from 192.168.57.1
it4@node1:~$
```

## 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

```bash
# Modification du fichier de conf
it4@node1:~$ sudo vim /etc/ssh/sshd_config
it4@node1:~$ cat /etc/ssh/sshd_config | grep Port
Port 7777
#GatewayPorts no

# Redémarrage du service pour que le changement soit effectif
it4@node1:~$ sudo systemctl restart ssh

# Vérification que le processus tourne derrière notre nouveau port
it4@node1:~$ sudo ss -alnpt | grep ssh
LISTEN    0         128                0.0.0.0:7777             0.0.0.0:*        users:(("sshd",pid=2051,fd=3))                                                 
LISTEN    0         128                   [::]:7777                [::]:*        users:(("sshd",pid=2051,fd=4))
```

🌞 **Connectez vous sur le nouveau port choisi**

```bash
# Vu que le port a changé, est n'est plus le port 22 qui est utilisé par défaut, par convention
# On a doit préciser avec -p le port où l'on veut se connecter
[it4@nowhere 2]$ ssh 192.168.57.8 -p 7777
it4@192.168.57.8's password: 
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

112 mises à jour peuvent être appliquées immédiatement.
51 de ces mises à jour sont des mises à jour de sécurité.
Pour afficher ces mises à jour supplémentaires, exécuter : apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Tue Nov 16 11:29:37 2021 from 192.168.57.1
it4@node1:~$ 
````

# Partie 2 : FTP

# II. Setup du serveur FTP

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

```bash
it4@node1:~$ sudo apt install -y vsftpd
[...]
Sélection du paquet vsftpd précédemment désélectionné.
(Lecture de la base de données... 208067 fichiers et répertoires déjà installés.)
Préparation du dépaquetage de .../vsftpd_3.0.3-12_amd64.deb ...
Dépaquetage de vsftpd (3.0.3-12) ...
Paramétrage de vsftpd (3.0.3-12) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Traitement des actions différées (« triggers ») pour man-db (2.9.1-1) ...
Traitement des actions différées (« triggers ») pour systemd (245.4-4ubuntu3.11) ...
```

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

```bash
# Démarrage du service vsftpd
it4@node1:~$ sudo systemctl start vsftpd

# Affichage du statut de vsftpd
it4@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-16 11:33:29 CET; 17min ago
   Main PID: 2309 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 528.0K
     CGroup: /system.slice/vsftpd.service
             └─2309 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 16 11:33:29 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 16 11:33:29 node1.tp2.linux systemd[1]: Started vsftpd FTP server.

# Activation du service au démarrage de la machine
it4@node1:~$ sudo systemctl enable vsftpd
Synchronizing state of vsftpd.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable vsftpd
```

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

```bash
# Statut du service
it4@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-16 11:33:29 CET; 19min ago
   Main PID: 2309 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 528.0K
     CGroup: /system.slice/vsftpd.service
             └─2309 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 16 11:33:29 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 16 11:33:29 node1.tp2.linux systemd[1]: Started vsftpd FTP server.

# Processus lancé par le service
it4@node1:~$ ps -ef | grep vsftpd
root        2309       1  0 11:33 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd.conf
it4         3020    2118  0 11:52 pts/1    00:00:00 grep --color=auto vsftpd

# Port derrière lequel le processus vsftpd s'exécute (on voit que c'est le port 21)
it4@node1:~$ sudo ss -alnpt | grep vsftpd
LISTEN    0         32                       *:21                     *:*        users:(("vsftpd",pid=2309,fd=3))                                               
# Visualisaton des logs
it4@node1:~$ sudo journalctl -xe -u vsftpd
-- Logs begin at Tue 2021-10-19 16:35:26 CEST, end at Tue 2021-11-16 11:52:58 CET. --
nov. 16 11:33:29 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
-- Subject: L'unité (unit) vsftpd.service a commencé à démarrer
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
-- 
-- L'unité (unit) vsftpd.service a commencé à démarrer.
nov. 16 11:33:29 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
-- Subject: L'unité (unit) vsftpd.service a terminé son démarrage
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
-- 
-- L'unité (unit) vsftpd.service a terminé son démarrage, avec le résultat done.
```

🌞 **Connectez vous au serveur**

Pour uploader un fichier, il faut ajouter une ligne à la config du serveur vsftpd :

```bash
# La ligne write_enable permet d'activer l'écriture (l'upload)
it4@node1:~$ cat /etc/vsftpd.conf   | grep write_enable
write_enable=YES
#anon_mkdir_write_enable=YES

# Redémarrage du service pour que le changement prenne effet
it4@node1:~$ sudo systemctl restart vsftpd
```

Pour activer le log lors de l'upload et le download, il faut encore modifier la configuration de vsftpd :

```bash
# On visualise la ligne à vérifier
it4@node1:~$ cat /etc/vsftpd.conf | grep xferlog_enable
xferlog_enable=YES

# Redémarrage du service si besoin
it4@node1:~$ sudo systemctl restart vsftpd
```

🌞 **Visualiser les logs**

```bash
# Logs d'upload et download
it4@node1:~$ sudo !!
sudo cat /var/log/vsftpd.log 
Tue Nov 16 11:56:55 2021 [pid 3084] CONNECT: Client "::ffff:192.168.57.1"
Tue Nov 16 11:56:58 2021 [pid 3083] [it4] OK LOGIN: Client "::ffff:192.168.57.1"
Tue Nov 16 11:58:53 2021 [pid 3102] CONNECT: Client "::ffff:192.168.57.1"
Tue Nov 16 11:58:53 2021 [pid 3101] [it4] OK LOGIN: Client "::ffff:192.168.57.1"
Tue Nov 16 11:59:02 2021 [pid 3105] CONNECT: Client "::ffff:192.168.57.1"
Tue Nov 16 11:59:02 2021 [pid 3104] [it4] OK LOGIN: Client "::ffff:192.168.57.1"
Tue Nov 16 11:59:02 2021 [pid 3106] [it4] OK UPLOAD: Client "::ffff:192.168.57.1", "/home/it4/id_rsa.pub", 741 bytes, 311.24Kbyte/sec
Tue Nov 16 12:00:31 2021 [pid 3109] CONNECT: Client "::ffff:192.168.57.1"
Tue Nov 16 12:00:31 2021 [pid 3108] [it4] OK LOGIN: Client "::ffff:192.168.57.1"
Tue Nov 16 12:00:31 2021 [pid 3110] [it4] OK DOWNLOAD: Client "::ffff:192.168.57.1", "/home/it4/yo", 3 bytes, 4.21Kbyte/sec
```

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

```bash
# Modification du fichier de conf
it4@node1:~$ sudo vim /etc/vsftpd.conf 
it4@node1:~$ cat /etc/vsftpd.conf | grep listen_port
listen_port=8888

# Redémarrage du service pour que notre changement prenne effet
it4@node1:~$ sudo systemctl restart vsftpd

# On visualise le changement en listant les ports en écoute, et en filtrant les lignes qui contiennent "vsftpd"
it4@node1:~$ sudo ss -alnpt | grep vsftpd
LISTEN    0         32                       *:8888                   *:*        users:(("vsftpd",pid=3145,fd=3))
```

🌞 **Connectez vous sur le nouveau port choisi**

Pour visualiser ça, voici les logs de connexion de mon FileZilla, une fois qu'on utilie le nouveau port :

```
Status:	Connecting to 192.168.57.8:8888...
Status:	Connection established, waiting for welcome message...
Status:	Insecure server, it does not support FTP over TLS.
Status:	Server does not support non-ASCII characters.
Status:	Logged in
```

Download et upload toujours fonctionnels, voyons les logs : 

```bash
it4@node1:~$ sudo tail -n 2 /var/log/vsftpd.log 
Tue Nov 16 12:13:45 2021 [pid 3167] [it4] OK UPLOAD: Client "::ffff:192.168.57.1", "/home/it4/known_hosts", 8876 bytes, 2984.84Kbyte/sec
Tue Nov 16 12:13:50 2021 [pid 3167] [it4] OK DOWNLOAD: Client "::ffff:192.168.57.1", "/home/it4/yo", 3 bytes, 3.79Kbyte/sec
```


# Partie 3 : Création de votre propre service

# II. Jouer avec netcat

🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**

- la commande tapée sur la VM

```bash
it4@node1:~$ nc -l 9999
hello
coucou
```

- la commande tapée sur votre PC

```bash
[it4@nowhere 2]$ nc 192.168.57.8 9999
hello
coucou
```

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

Sur la VM : 

```bash
it4@node1:~$ nc -l 9999 > /tmp/messages_echanges
# on note que rien ne s'affiche dans le terminal

# tous les messages du client ont été redirigé vers un fichier
it4@node1:~$ cat /tmp/messages_echanges
coucou
hello
```

Sur le PC :

```bash
[it4@nowhere 2]$ nc 192.168.57.8 9999
coucou
hello
```

# III. Un service basé sur netcat

## 1. Créer le service

🌞 **Créer un nouveau service**

```bash
# On détermine le chemin où est stocké la commande nc
it4@node1:~$ which nc
/usr/bin/nc

# On crée le service
it4@node1:~$ sudo vim /etc/systemd/system/chat_tp2.service
it4@node1:~$ cat /etc/systemd/system/chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 9999

[Install]
WantedBy=multi-user.target

# On indique au système qu'on a modifié le répertoire /etc/systemd/system
it4@node1:~$ sudo systemctl daemon-reload

```

## 2. Test test et retest

🌞 **Tester le nouveau service**

```bash
it4@node1:~$ which nc
/usr/bin/nc
it4@node1:~$ sudo vim /etc/systemd/system/chat_tp2.service
it4@node1:~$ cat /etc/systemd/system/chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 9999

[Install]
WantedBy=multi-user.target
it4@node1:~$ sudo systemctl daemon-reload
suit4@node1:~$ sudo systemctl start chat_tp2
it4@node1:~$ systemctl status  chat_tp2
● chat_tp2.service - Little chat service (TP2)
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-16 12:30:27 CET; 3s ago
   Main PID: 3251 (nc)
      Tasks: 1 (limit: 2312)
     Memory: 192.0K
     CGroup: /system.slice/chat_tp2.service
             └─3251 /usr/bin/nc -l 9999

nov. 16 12:30:27 node1.tp2.linux systemd[1]: Started Little chat service (TP2).

# Vérification du port utilisé
it4@node1:~$ sudo ss -alnpt | grep nc
LISTEN    0         1                  0.0.0.0:9999             0.0.0.0:*        users:(("nc",pid=3251,fd=3))
```

Depuis le client, on s'y connecte :

```bash
[it4@nowhere 2]$ nc 192.168.57.8 9999
hello
MEOW
```

Depuis la VM : on visualise l'état du service et les messages envoyés par le client :

```bash
# La commande journalctl affiche tout ce qui est sorti de la commande nc
# Ceci contient notamment les messages de notre client
# On voit dans les logs les messages "hello" et "MEOW"
it4@node1:~$ journalctl -xe -u chat_tp2
-- Logs begin at Tue 2021-10-19 16:35:26 CEST, end at Tue 2021-11-16 12:33:19 CET. --
nov. 16 12:30:27 node1.tp2.linux systemd[1]: Started Little chat service (TP2).
-- Subject: L'unité (unit) chat_tp2.service a terminé son démarrage
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
-- 
-- L'unité (unit) chat_tp2.service a terminé son démarrage, avec le résultat done.
nov. 16 12:33:15 node1.tp2.linux nc[3251]: hello
nov. 16 12:33:17 node1.tp2.linux nc[3251]: MEOW
nov. 16 12:33:19 node1.tp2.linux systemd[1]: chat_tp2.service: Succeeded.
-- Subject: Unit succeeded
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
-- 
-- The unit chat_tp2.service has successfully entered the 'dead' state.
```
