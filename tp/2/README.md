# TP2 : Manipulation de services

- [TP2 : Manipulation de services](#tp2--manipulation-de-services)
- [Intro](#intro)
- [Prérequis](#prérequis)
  - [1. Une machine xubuntu fonctionnelle](#1-une-machine-xubuntu-fonctionnelle)
  - [2. Nommer la machine](#2-nommer-la-machine)
  - [3. Config réseau](#3-config-réseau)
- [Go next](#go-next)

Dans nos cours, on ne va que peu s'attarder sur l'aspect client des systèmes GNU/Linux, mais plutôt sur la façon dont on le manipule en tant qu'admin.

Ca permettra aussi, *via* la manipulation, d'appréhender un peu mieux comment un OS de ce genre fonctionne.

# Intro

Dans ce TP on va s'intéresser aux *services* de la machine. Un *service* c'est un processus dont l'OS s'occupe. 

Plutôt que de le lancer à la main, on demande à l'OS de le gérer, c'est moins chiant !

> **Par exemple, quand vous ouvrez votre PC, vous lancez pas une commande pour avoir une interface graphique si ?** L'interface graphique, elle est juste là, elle pop "toute seule". En vérité, c'est l'OS qui l'a lancée. L'interface graphique est donc un *service*.

Souvent un service...

- bon bah c'est un processus qui s'exécute
  - c'est le système qui a fait en sorte qu'il se lance
  - le système a la charge du processus
  - genre il va le relancer si le processus crash par exemple
- souvent il a un fichier de configuration
  - ça nous permet de le paramétrer
  - les changements prennent effet quand on redémarre le service
- souvent on peut définir sous quelle identité le service va tourner
  - genre on désigne un utilisateur du système qui lancera le processus
  - c'est cet utilisateur qui s'affichera dans la liste des processus
- si c'est un service qui utilise le réseau (comme SSH, HTTP, FTP, autres.) il écoute sur un port

# Prérequis

> Y'a toujours une section prérequis dans mes TPs, ça vous sert à préparer l'environnement pour réaliser le TP. Dans ce TP, c'est le premier avec des prérequis, alors je vais détailler le principe et vous devrez me rendre la réalisation de ces étapes dans le compte rendu.

## 1. Une machine xubuntu fonctionnelle

N'hésitez pas à cloner celle qu'on a créé ensemble.

**Pour TOUTES les commandes que je vous donne dans le TP**

- elles sont dans le [memo de commandes Linux](../../cours/memos/commandes.md)
- vous **DEVEZ** consulter le `help` au minimum voire le `man` ou faire une recherche Internet pour comprendre comment fonctionne la commande

```bash
# Consulter le help de ls
$ ls --help

# Consulter le manuel de ls
$ man ls
```

## 2. Nommer la machine

➜ **On va renommer la machine**

- parce qu'on s'y retrouve plus facilement
- parce que toutes les machines sont nommées dans la vie réelle, pour cette raison, alors habituez vous à le faire systématiquement :)

On désignera la machine par le nom `node1.tp2.linux`

🌞 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**
  - taper la commande `sudo hostname <NOM_MACHINE>`
  - pour vérifier le changement, vous pouvez ouvrir un nouveau terminal, et observer le changement dans le prompt du terminal
- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**
  - il faut inscrire le nom de la machine dans le fichier `/etc/hostname`
  - pour vérifier le changement, il faut redémarrer la machine
- je veux les deux étapes dans le compte-rendu

## 3. Config réseau

➜ **Vérifiez avant de continuer le TP que la configuration réseau de la machine est OK. C'est à dire :**

- la machine doit pouvoir joindre internet
- votre PC doit pouvoir `ping` la machine

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash
# Affichez la liste des cartes réseau de la machine virtuelle
# Vérifiez que les cartes réseau ont toute une IP
$ ip a

# Si les cartes n'ont pas d'IP vous pouvez les allumez avec la commande
$ nmcli con up <NOM_INTERFACE>
# Par exemple
$ nmcli con up enp0s3

# Vous devez repérer l'adresse de la VM dans le host-only

# Vous pouvez tester de ping un serveur connu sur internet
# On teste souvent avec 1.1.1.1 (serveur DNS de CloudFlare)
# ou 8.8.8.8 (serveur DNS de Google)
$ ping 1.1.1.1

# On teste si la machine sait résoudre des noms de domaine
$ ping ynov.com
```

Ensuite on vérifie que notre PC peut `ping` la machine (étapes à réaliser SUR VOTRE PC) :

```bash
# Afficher la liste de vos carte réseau, la commande dépend de votre OS
$ ip a # Linux
$ ipconfig # Windows
$ ifconfig # MacOS

# Vous devez repérer l'adresse de votre PC dans le host-only

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>
```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  - depuis la VM : `ping 1.1.1.1` fonctionnel
  - depuis la VM : `ping ynov.com` fonctionnel
  - depuis votre PC : `ping <IP_VM>` fonctionnel

# Go next

Vous pouvez ensuite vous attaquer aux 3 parties du TP. Elles sont indépendantes mais je vous recommande de les faire dans l'ordre.

> Si ce que je vous demande de faire vous paraît flou, difficile, incompréhensible, si vous savez pas par où commencer ou comment vous y prendre. DEMANDEZ-MOI JE SUIS LA POUR CA. :)

- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
- [Partie 3 : Création de votre propre service](./part3.md)

![HF](./pics/have-fun-but-not-too-much.jpg)