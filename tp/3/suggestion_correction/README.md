# TP 3 : A little script

- [TP 3 : A little script](#tp-3--a-little-script)
- [I. Script carte d'identité](#i-script-carte-didentité)
- [II. Script youtube-dl](#ii-script-youtube-dl)
- [III. MAKE IT A SERVICE](#iii-make-it-a-service)

# I. Script carte d'identité

📁 **Fichier [`/srv/idcard/idcard.sh`](./scripts/idcard.sh)**

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```bash
it4@node1:/srv/idcard$ sudo ./idcard.sh 
Machine name : node1.tp2.linux
OS is Ubuntu 20.04.3 LTS and kernel version is 5.11.0-40-generic
IP : 192.168.57.8/24
RAM : 1,2Gi/1,9Gi
Disque : 966M space left
Top 5 processes by RAM usage :
  - /usr/bin/python3 (ID 1300) : 3.8%
  - /usr/libexec/fwupd/fwupd (ID 19465) : 3.7%
  - /usr/lib/xorg/Xorg (ID 618) : 2.7%
  - xfwm4 (ID 1012) : 2.7%
  - /usr/bin/python3 (ID 1125) : 1.5%
Listening ports :
  - 53 : systemd-resolve
  - 631 : cupsd
  - 7777 : sshd
  - 631 : cupsd
  - 8888 : vsftpd
  - 7777 : sshd
Here's your random cat : "https://cdn2.thecatapi.com/images/cb1.jpg"
```

# II. Script youtube-dl

📁 **Le script [`/srv/yt/yt.sh`](./scripts/yt.sh)**

📁 **Le fichier de log `/var/log/yt/download.log`**, avec au moins quelques lignes

```bash
it4@node1:/srv/yt$ cat /var/log/yt/download.log
[21/12/02 16:12:31] Video jingle bells was downloaded. File path : /srv/yt/downloads/jingle bells/jingle bells.mkv
[21/12/02 17:12:33] Video jingle bells was downloaded. File path : /srv/yt/downloads/jingle bells/jingle bells.mkv
[21/12/02 17:12:12] Video jingle bells was downloaded. File path : /srv/yt/downloads/jingle bells/jingle bells.mkv
[21/12/02 17:12:08] Video jingle bells was downloaded. File path : /srv/yt/downloads/jingle bells/jingle bells.mkv
```



🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```bash
it4@node1:/srv/yt$ ./yt.sh https://www.youtube.com/watch?v=iGz0ReeZbww
[INFO] Video destination directory '/srv/yt/downloads/jingle bells' created.
[OK] Video https://www.youtube.com/watch?v=iGz0ReeZbww was downloaded.
[OK] File path : /srv/yt/downloads/jingle bells/jingle bells.mkv
```

# III. MAKE IT A SERVICE

📁 **Le script [`/srv/yt/yt-v2.sh`](./scripts/yt-v2.sh)**

📁 **Fichier `/etc/systemd/system/yt.service`**

```bash
it4@node1:/srv/yt$ sudo systemctl cat yt
# /etc/systemd/system/yt.service
[Unit]
Description=Service de téléchargement de vidéo Youtube

[Service]
ExecStart=/srv/yt/yt-v2.sh

[Install]
WantedBy=multi-user.target
```

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :

```bash
# On active le service au démarrage de la machine
it4@node1:/srv/yt$ sudo systemctl enable yt
Created symlink /etc/systemd/system/multi-user.target.wants/yt.service → /etc/systemd/system/yt.service.

# Démarrage manuel du service
it4@node1:/srv/yt$ sudo systemctl start yt

# Un p'tit status
it4@node1:/srv/yt$ sudo systemctl status yt
● yt.service - Service de téléchargement de vidéo Youtube
     Loaded: loaded (/etc/systemd/system/yt.service; enabled; vendor preset: enabl>
     Active: active (running) since Thu 2021-12-02 16:48:04 CET; 56s ago
   Main PID: 23063 (yt-v2.sh)
      Tasks: 2 (limit: 2299)
     Memory: 584.0K
     CGroup: /system.slice/yt.service
             ├─23063 /bin/bash /srv/yt/yt-v2.sh
             └─23119 sleep 5

déc. 02 16:48:04 node1.tp2.linux systemd[1]: Started Service de téléchargement de
```
🌟**BONUS** : get fancy. 

[![asciicast](https://asciinema.org/a/U2z2N9THPRBD7KVuOSDQlk59I.svg)](https://asciinema.org/a/U2z2N9THPRBD7KVuOSDQlk59I)
