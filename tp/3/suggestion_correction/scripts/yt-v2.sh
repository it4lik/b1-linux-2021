#!/bin/bash
# Download youtube videos listed in a file. Run as daemon.
# it4

# Script vars
log_dir='/var/log/yt'
log_file="${log_dir}/download.log"
video_list_file='/srv/yt/links' # This file will be filled with Youtube video URLs by users
yt_root="/srv/yt"
declare -i interval=5 # Duration (in seconds) between two parsings of the file

# Binary paths
youtube_dl='/home/it4/.local/bin/youtube-dl'

usage() {
echo "Usage : yt.sh [OPTION] YOUTUBE_VIDEO_URL
Download given Youtube video : YOUTUBE_VIDEO_URL

  -o         Defines a different YT root directory (defaults to ${yt_root})
  -h         Prints help message (this message)"
}

# Script options handling
while getopts ":ho:" option; do
    case "${option}" in
        h)
            usage
	    exit 0
            ;;
        o)
            yt_root=${OPTARG}
            ;;
        *)
	    log error "Option ${option} not recognized."
            usage
	    exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Preflight checks
if [[ ! -f "${video_list_file}" ]]
then
  echo "Video list file ${video_list_file} does not exist."
  exit 1
fi

if [[ ! -d "${yt_root}" ]]
then
  echo "YT root dir ${yt_root} does not exist."
  exit 1
fi

downloads_dir="${yt_root}/downloads"
if [[ ! -d "${downloads_dir}" ]]
then
  echo "Videos destination directory ${downloads_dir} does not exist."
  if mkdir "${downloads_dir}" 2> /dev/null ; then
    echo "Videos destination directory ${downloads_dir} created."
  else
    echo "Cannot create Videos destination directory ${downloads_dir}. Exiting."
    exit 1
  fi
fi

if [[ ! -d "${log_dir}" ]]
then
  echo "Log dir ${log_dir} does not exist."
  exit 1
fi

if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

# Infinite loop : the script will act as a daemon (background process)
while :
do 
  # Test if the file is empty
  # Sleep ${interval} seconds if it is
  while ! grep -q '[^[:space:]]' < "${video_list_file}"
  do
    #echo "Video list file ${video_list_file} is empty."
    #echo "Waiting ${interval} seconds."
    sleep ${interval}
    continue
  done

  # Grab the first line of ${video_list_file} in a variable
  youtube_video_url="$(head -n1 ${video_list_file})"

  # Sometimes the script is a bit fast if someone is writing a lot of URLs in ${video_list_file}
  # ${youtube_video_url} stays empty in some cases
  if [[ -z ${youtube_video_url} ]]
  then
    continue
  fi
  
  # Test if the first line of ${video_list_file} is a valid Youtube video URL
  youtube_youtube_video_url_regex='^https:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9]|-|_)*$'
  if [[ ! "${youtube_video_url}" =~ $youtube_youtube_video_url_regex ]]
  then
    echo "Given URL is not a valid Youtube video URL : ${youtube_video_url}"
    echo "Deleting first line of ${video_list_file}"
    sed -i '1d' "${video_list_file}"
  fi

  # Get the title of the given video
  video_title=$(${youtube_dl} --get-title "${youtube_video_url}")
  
  # Handle the downloaded video destination directory
  # A new directory is created (its name is the name of the downloaded video)
  video_dir="${downloads_dir}/${video_title}"
  if [[ -d "${video_dir}" ]]; then
    echo "Video destination directory '${video_dir}' already exists."
  else
    if mkdir "${video_dir}" &> /dev/null ; then
      echo "Video destination directory '${video_dir}' created."
    else
      echo "Cannot create video destination directory '${video_dir}'. Exiting."
      exit 1
    fi
  fi
  
  # Download the video descripton
  ${youtube_dl} "${youtube_video_url}" --get-description > "${video_dir}/description" 2> /dev/null
  
  # Get filename of the downloaded file
  downloaded_file_path=$(${youtube_dl} --get-filename "${youtube_video_url}" --output "${video_dir}/%(title)s" 2> /dev/null)
  
  # Actually downloads the video
  ${youtube_dl} "${youtube_video_url}" --output "${video_dir}/%(title)s" &> /dev/null
  
  # Detect video extension
  if [[ -f "${downloaded_file_path}.mp4" ]] ; then downloaded_file_path="${downloaded_file_path}.mp4"
  elif [[ -f "${downloaded_file_path}.mkv" ]] ; then downloaded_file_path="${downloaded_file_path}.mkv"
  elif [[ -f "${downloaded_file_path}.webm" ]] ; then downloaded_file_path="${downloaded_file_path}.webm"
  fi

  # Prompt success
  echo "Video ${youtube_video_url} was downloaded."
  echo "File path : ${downloaded_file_path}"
 
  # Write in logfile
  log_prefix=$(date +"[%y/%m/%d %H:%M:%S]")
  log_line="${log_prefix} Video ${video_title} was downloaded. File path : ${downloaded_file_path}"
  echo "${log_line}" >> "${log_file}"

  # Delete the link of the just downloaded video from ${video_list_file} file
  sed -i '1d' "${video_list_file}"
done
