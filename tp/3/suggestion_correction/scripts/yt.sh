#!/bin/bash
# Download youtube video passed as argument
# it4

# Colors & formatting
NC="\e[0m"   # reset formatting
R="\e[0;31m" # red
G="\e[0;32m" # green
Y="\e[0;33m" # yellow
B="\e[1m"    # bold

# Script vars
log_dir='/var/log/yt'
log_file="${log_dir}/download.log"
yt_root="/srv/yt"

# Binary paths
youtube_dl='/home/it4/.local/bin/youtube-dl'

# General functions
log() {
  log_level=$1
  log_message=$2

  if [[ "${log_level}" == 'error' ]] ; then format="${R}${B}[ERROR]${NC}"
  elif [[ "${log_level}" == 'warning' ]] ; then format="${Y}${B}[WARNING]${NC}"
  elif [[ "${log_level}" == 'info' ]] ; then format="${B}[INFO]${NC}"
  elif [[ "${log_level}" == 'success' ]] ; then format="${G}${B}[OK]${NC}"
  fi

  echo -e "${format} ${log_message}"
}

usage() {
echo "Usage : yt.sh [OPTION] YOUTUBE_VIDEO_URL
Download given Youtube video : YOUTUBE_VIDEO_URL

  -o         Defines a different YT root directory (defaults to /srv/yt/)
  -h         Prints help message (this message)"
}

# Script options handling
while getopts ":ho:" option; do
    case "${option}" in
        h)
            usage
	    exit 0
            ;;
        o)
            yt_root=${OPTARG}
            ;;
        *)
	    log error "Option ${option} not recognized."
            usage
	    exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Preflight checks
if [[ ! -d "${yt_root}" ]]
then
  log error "YT root dir ${yt_root} does not exist."
  exit 1
fi

downloads_dir="${yt_root}/downloads"
if [[ ! -d "${downloads_dir}" ]]
then
  log info "Videos destination directory ${downloads_dir} does not exist."
  if mkdir "${downloads_dir}" 2> /dev/null ; then
    log info "Videos destination directory ${downloads_dir} created."
  else
    log error "Cannot create Videos destination directory ${downloads_dir}. Exiting."
    exit 1
  fi
fi

if [[ ! -d "${log_dir}" ]]
then
  log error "Log dir ${log_dir} does not exist."
  exit 1
fi

if [[ ! -w "${log_dir}" ]]
then
  log error "Log dir ${log_dir} is not writable."
  exit 1
fi

# Args handling
youtube_video_url="$1"

# Test if arg is a valid Youtube URL
youtube_youtube_video_url_regex='^https:\/\/www\.youtube\.com\/watch\?v=([a-zA-Z0-9]|-|_)*$'
if [[ -z ${youtube_video_url} ]]
then
  log error "You did not specify an URL as argument."
  exit 1
fi

if [[ ! "${youtube_video_url}" =~ $youtube_youtube_video_url_regex ]]
then
  log error "Given URL is not a valid Youtube video URL : ${youtube_video_url}"
  exit 1
fi

# Get the title of the given video
video_title=$(${youtube_dl} --get-title "${youtube_video_url}")

# Handle destination directory
video_dir="${downloads_dir}/${video_title}"
if [[ -d "${video_dir}" ]]; then
  log info "Video destination directory '${video_dir}' already exists."
else
  if mkdir -p "${video_dir}" 2> /dev/null ; then
    log info "Video destination directory '${video_dir}' created."
  else
    log error "Cannot create destination directory '${video_dir}'. Exiting."
    exit 1
  fi
fi

# Download the video descripton
${youtube_dl} "${youtube_video_url}" --get-description > "${video_dir}/description" 2> /dev/null

# Get filename of the downloaded file
downloaded_file_path=$(${youtube_dl} --get-filename "${youtube_video_url}" --output "${video_dir}/%(title)s" 2> /dev/null)

# Actually downloads the video
${youtube_dl} "${youtube_video_url}" --output "${video_dir}/%(title)s" &> /dev/null

# Detect video extension
if [[ -f "${downloaded_file_path}.mp4" ]] ; then downloaded_file_path="${downloaded_file_path}.mp4"
elif [[ -f "${downloaded_file_path}.mkv" ]] ; then downloaded_file_path="${downloaded_file_path}.mkv"
elif [[ -f "${downloaded_file_path}.webm" ]] ; then downloaded_file_path="${downloaded_file_path}.webm"
fi

# Prompt success
log success "Video ${youtube_video_url} was downloaded."
log success "File path : ${downloaded_file_path}"

# Write in logfile
log_prefix=$(date +"[%y/%/%d %H:%M:%S]")
log_line="${log_prefix} Video ${video_title} was downloaded. File path : ${downloaded_file_path}"
echo "${log_line}" >> "${log_file}"
