#!/bin/bash
# Simple script that retrieves and display generic infos about the system
# it4

if [[ "$(id -u)" != "0" ]] ; then
	echo "This script must be run as root."
	exit 1
fi

# Machine Name
id_hostname="$(hostname)"
echo "Machine name : $id_hostname"

# System OS and kernel
source /etc/os-release
id_os="${PRETTY_NAME}"
id_kernel="$(uname -r)"
echo "OS is $id_os and kernel version is $id_kernel"

# IP address
## Je vous mets deux façons de faire pour l'IP :
## Façon 1 : on utilise awk
## et awk s'en fout s'il y a plusieurs espaces qui se suivent
id_ip="$(ip a show enp0s9 | grep "inet " | awk '{ print $2 }')"

## Façon 2 : on utilise cut
## Mais on utilise tr avant, pour supprimer les espaces quand plusieurs espaces se suivent
id_ip="$(ip a show enp0s9 | grep "inet "  | tr -s ' ' | cut -d' ' -f3)"
echo "IP : $id_ip"

# Memory
id_total_mem="$(free -mh | grep Mem: | tr -s ' ' | cut -d' ' -f2)"
id_available_mem="$(free -mh | grep Mem: | tr -s ' ' | cut -d' ' -f7)"
echo "RAM : $id_available_mem/$id_total_mem"

# Disk usage
id_available_disk="$(df -h | grep -E '/$' | tr -s ' ' | cut -d' ' -f4)"
echo "Disque : $id_available_disk space left"

# Top 5 processes by RAM usage
echo "Top 5 processes by RAM usage :"
top_5_process="$(ps -eo %mem,pid,cmd --sort -rss | grep -v CMD  | head -n 5)"
echo "$top_5_process" | while read process
do
	process=$(echo $process | tr -s ' ')
	process_mem=$(echo $process | cut -d' ' -f1)
	process_id=$(echo $process | cut -d' ' -f2)
	process_name=$(echo $process | cut -d' ' -f3)
	echo "  - $process_name (ID $process_id) : ${process_mem}%"
done

# Listening ports
echo "Listening ports :"
listening_services="$(ss -alnpt | grep -v ^State | tr -s ' ')"
echo "$listening_services" | while read service
do
  port=$(echo "$service" | cut -d' ' -f4 | rev | cut -d':' -f1 | rev)
  program=$(echo "$service" | cut -d' ' -f6 | cut -d '"' -f2)
  echo "  - $port : $program"
done

# Random cat pic
id_cat_url="$(curl https://api.thecatapi.com/v1/images/search --silent | jq '.[].url')"
echo "Here's your random cat : $id_cat_url"
