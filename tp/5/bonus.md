# Bonus

➜ **Mettre en place du HTTPS sur le serveur web**

Ainsi, l'accès au site web sera confidentiel, et votre serveur sera en mesure de vous prouver son identité.
Il est aussi possible de rediriger tout le trafic HTTP vers du trafic HTTPS.

➜ **Mettre en place de la répartition de charge**

L'idée est de redonder le serveur Web. Plutôt que d'avoir un Apache, on en met deux, sur deux VMs différentes.
Il est alors possible de mettre en place une troisième machine, en plus des deux serveurs web, qui agira comme reverse proxy.
Le rôle du reverse proxy est d'accueillir les requêtes des clients, puis de les répartir sur les serveurs web.

➜ **Mettre en place [Netdata](https://www.netdata.cloud/) sur les deux noeuds**

Pour avoir un monitoring élémentaire.
Une fois Netdata opérationnel, configurez-le pour qu'il vous envoie des alertes (Discord par exemple).

➜ **Mettre en place une sauvegarde de la base de données**

A l'aide d'un script `bash` qui utilise la commande `mysqldump`, effectuez des sauvegardes réglières de la base de données.  
On peut imaginer un service qui s'occuperait de lancer ce script à intervalles réguliers.
